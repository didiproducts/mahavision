package com.mahavision;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class IconView extends View {

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mBitmapPaint;
    private boolean[][] tilesCoords;
    private boolean canDraw = true;
    private int size;
    private Bitmap black;
    private float mX, mY;

    public IconView(Context c, boolean[][] tilesCoords, boolean canDraw) {
        super(c);
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        this.tilesCoords = tilesCoords;
        this.canDraw = canDraw;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        size = w;
        black = Bitmap.createBitmap(size / 10, size / 10, Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        redrawCanvas();
    }

    private void redrawCanvas() {
        mBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        for (int i = 0; i < black.getHeight(); i++) {
            for (int j = 0; j < black.getHeight(); j++) {
                black.setPixel(i, j, Color.BLACK);
            }
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (tilesCoords[i][j]) {
                    mCanvas.drawBitmap(black, size / 10 * i, size / 10 * j, null);
                }
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
    }

    private void touch_start(float x, float y) {
        mX = x;
        mY = y;

        if (x / (size / 10) < 10 && y / (size / 10) < 10)
            tilesCoords[(int) (x / (size / 10))][(int) (y / (size / 10))] = !tilesCoords[(int) (x / (size / 10))][(int) (y / (size / 10))];
        redrawCanvas();
    }

    private void touch_move(float x, float y) {
        if ((Math.abs((x / (size / 10)) - (mX / (size / 10))) > 1) || (Math.abs((y / (size / 10)) - (mY / (size / 10))) > 1)) {
            mX = x;
            mY = y;

            if ((x > 0) && (y > 0) && (x / (size / 10) < 10 && y / (size / 10) < 10))
                tilesCoords[(int) (x / (size / 10))][(int) (y / (size / 10))] = !tilesCoords[(int) (x / (size / 10))][(int) (y / (size / 10))];
            redrawCanvas();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (canDraw) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    //touch_up();
                    invalidate();
                    break;
            }
        }
        return canDraw;
    }
}

