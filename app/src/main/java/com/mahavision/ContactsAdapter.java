package com.mahavision;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactHolder> {

    private Context context;
    private ArrayList<Contact> mContactsItems;

    public ContactsAdapter(Context context, ArrayList<Contact> contacts) {
        this.context = context;
        this.mContactsItems = contacts;
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false);
        return new ContactsAdapter.ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsAdapter.ContactHolder holder, int position) {
        holder.bind(mContactsItems.get(position));
    }


    @Override
    public int getItemCount() {
        return mContactsItems.size();
    }

    class ContactHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private LinearLayout iconPlaceHolder;
        private IconView iconView;
        boolean[][] tilesCoords = new boolean[10][10];

        // icon..

        ContactHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            iconPlaceHolder = itemView.findViewById(R.id.iconPlaceholder);
        }

        void bind(final Contact contact) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("בחר פעולה");

                    String[] animals = {"התקשר", "שלח מייל", "הוסף לאנשי קשר"};
                    builder.setItems(animals, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(Intent.ACTION_CALL);
                                    intent.setData(Uri.parse("tel:" + contact.getPhoneNumber()));
                                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions((Activity) context,
                                                new String[]{Manifest.permission.CALL_PHONE},
                                                1);
                                        return;
                                    }
                                    context.startActivity(intent);
                                    break;
                                case 1:
                                    String[] TO = {contact.getEmail()};
                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                    emailIntent.setData(Uri.parse("mailto:"));
                                    emailIntent.setType("text/plain");
                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "מחנ\"ט 2018");
                                    emailIntent.putExtra(Intent.EXTRA_TEXT, " ");
                                    try {
                                        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                                    } catch (android.content.ActivityNotFoundException ex) {
                                        Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case 2:
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, contact.getFullName())
                                            .putExtra(ContactsContract.Intents.Insert.COMPANY, "מחנט")
                                            .putExtra(ContactsContract.Intents.Insert.EMAIL, contact.getEmail())
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, contact.getPhoneNumber());

                                    context.startActivity(contactIntent);
                                    break;

                            }
                        }
                    });
                    builder.create().show();
                }
            });

            tvName.setText(contact.getFullName());
            initIcon(contact);
        }


        private void initIcon(Contact contact) {

            iconPlaceHolder.removeAllViews();

            if (SharedPref.getInt("icon_size", 0) != 0) {
                tilesCoords = convertStringToBinary(contact.getIcon());
            } else {
                boolean flag = false;
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {
                        tilesCoords[i][j] = flag;
                        flag = !flag;
                    }
                    flag = !flag;
                }

                SharedPref.putArray(tilesCoords, "icon");
            }

            iconView = new IconView(context, tilesCoords, false);

            iconPlaceHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View iconView = new IconView(context, tilesCoords, false);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.weight = 1.0f;
                    params.gravity = Gravity.CENTER;
                    params.setMargins(100, 100, 100, 0);
                    iconView.setLayoutParams(params);
                    LinearLayout linearLayout = new LinearLayout(context);
                    linearLayout.addView(iconView);
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(context));
                    dialogBuilder.setView(linearLayout);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            alertDialog.getWindow().setLayout(Objects.requireNonNull(alertDialog.getWindow()).getDecorView().getWidth(), alertDialog.getWindow().getDecorView().getWidth()); //Controlling width and height.
                        }
                    }, 100);
                }
            });
            iconPlaceHolder.addView(iconView);
        }

        private boolean[][] convertStringToBinary(String icon) {
            boolean[][] tilesCoords = new boolean[10][10];
            char[] charArray = icon.toCharArray();

            for (int i = 0; i < charArray.length / 10; i++)
                for (int j = 0; j < charArray.length / 10; j++) {
                    try {
                        tilesCoords[i][j] = charArray[i * j] == '1';
                    } catch (Exception e) {
                        Log.d("d", "dsad");
                    }
                }

            return tilesCoords;
        }
    }

}
