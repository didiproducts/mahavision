package com.mahavision.fragment;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mahavision.BluetoothLeService;
import com.mahavision.DeviceControlActivity;
import com.mahavision.IconView;
import com.mahavision.LoginActivity;
import com.mahavision.R;
import com.mahavision.SharedPref;

import java.util.Objects;
import java.util.UUID;

import static android.content.Context.BIND_AUTO_CREATE;
import static com.mahavision.DeviceControlActivity.EXTRAS_DEVICE_ADDRESS;
import static com.mahavision.DeviceControlActivity.EXTRAS_DEVICE_NAME;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    boolean[][] tilesCoords = new boolean[10][10];
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText phoneNumber;
    private LinearLayout iconPlaceHolder;
    private View rootView;
    private boolean mConnected = false;
    private String mDeviceName;
    private String mDeviceAddress;
    private BluetoothLeService mBluetoothLeService;
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                Objects.requireNonNull(getActivity()).finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                Toast.makeText(context, "ניתוק מהמשקפיים", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                Objects.requireNonNull(getActivity()).finish();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
//                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    Boolean synced;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        final Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        Intent gattServiceIntent = new Intent(getActivity(), BluetoothLeService.class);
        Objects.requireNonNull(getActivity()).bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        firstName = rootView.findViewById(R.id.firstName);
        lastName = rootView.findViewById(R.id.lastName);
        email = rootView.findViewById(R.id.email);
        phoneNumber = rootView.findViewById(R.id.phoneNumber);
        Button saveB = rootView.findViewById(R.id.saveB);
        iconPlaceHolder = rootView.findViewById(R.id.iconPlaceholder);

        firstName.setText(SharedPref.getString("firstName", ""));
        lastName.setText(SharedPref.getString("lastName", ""));
        email.setText(SharedPref.getString("email", ""));
        phoneNumber.setText(SharedPref.getString("phoneNumber", ""));

        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.putString("firstName", firstName.getText().toString());
                SharedPref.putString("lastName", lastName.getText().toString());
                SharedPref.putString("email", email.getText().toString());
                SharedPref.putString("phoneNumber", phoneNumber.getText().toString());

                // Sync to server and glasses

                UUID firstNameUUID = UUID.fromString("46495253-5430-4e41-4d45-303030303030");

                synced = mBluetoothLeService.writeCharacteristic(firstNameUUID, firstName.getText().toString());

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UUID lastNameUUID = UUID.fromString("4c415354-304e-414d-4530-303030303030");
                        synced = synced && mBluetoothLeService.writeCharacteristic(lastNameUUID, lastName.getText().toString());
                        if (synced) {
                            SharedPref.putString("firstName", firstName.getText().toString());
                            SharedPref.putString("lastName", lastName.getText().toString());
                            SharedPref.putString("email", email.getText().toString());
                            SharedPref.putString("phoneNumber", phoneNumber.getText().toString());
                            Toast.makeText(mBluetoothLeService, "סונכרן בהצלחה", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(mBluetoothLeService, "לא סונכרן :(", Toast.LENGTH_SHORT).show();
                    }
                }, 500);


                UUID idUUID = UUID.fromString("64696469-0000-1000-8000-00805f9b34fb");
                // mBluetoothLeService.writeCharacteristic(idUUID, id);
            }
        });

        iconPlaceHolder.setOnClickListener(this);

        initIcon();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(getActivity()).registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    public void onPause() {
        super.onPause();
        Objects.requireNonNull(getActivity()).unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void initIcon() {
        iconPlaceHolder.removeAllViews();

        if (SharedPref.getInt("icon_size", 0) != 0) {
            tilesCoords = SharedPref.getArray("icon");
        } else {
            boolean flag = false;
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    tilesCoords[i][j] = flag;
                    flag = !flag;
                }
                flag = !flag;
            }

            SharedPref.putArray(tilesCoords, "icon");
        }

        IconView iconView = new IconView(getActivity(), tilesCoords, false);
        iconView.setOnClickListener(this);
        iconPlaceHolder.addView(iconView);
    }

    @Override
    public void onClick(View v) {
        View iconView = new IconView(getActivity(), tilesCoords, true);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1.0f;
        params.gravity = Gravity.CENTER;
        params.setMargins(100, 100, 100, 0);
        iconView.setLayoutParams(params);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.addView(iconView);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        dialogBuilder.setView(linearLayout);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                SharedPref.putArray(tilesCoords, "icon");
                initIcon();
            }
        });


        alertDialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                alertDialog.getWindow().setLayout(alertDialog.getWindow().getDecorView().getWidth(), alertDialog.getWindow().getDecorView().getWidth()); //Controlling width and height.
            }
        }, 100);
    }
}
