package com.mahavision.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahavision.Contact;
import com.mahavision.ContactsAdapter;
import com.mahavision.R;

import java.util.ArrayList;

public class ContactsFragment extends Fragment {

    private RecyclerView mRecycler;
    private ContactsAdapter mContactsAdapter;
    private LinearLayoutManager mManager;
    private ArrayList<Contact> contacts;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);

        mRecycler = rootView.findViewById(R.id.contacts_list);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(mManager);

        contacts = new ArrayList<>();
        contacts.add(new Contact("id1", "פרטי", "משפחה", "nirel.didi@gmail.com", "0526573785", "1010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010"));
        contacts.add(new Contact("id1", "פרטי2", "משפחה2", "e1", "pn1", "0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101"));
        mContactsAdapter = new ContactsAdapter(getActivity(), contacts);

        mRecycler.setAdapter(mContactsAdapter);
        return rootView;
    }
}
