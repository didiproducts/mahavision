package com.mahavision;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SharedPref {

    private static SharedPreferences sharedPref;
    private static Gson gson;

    public static void init(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        gson = new Gson();
    }

    public static void putInteger(String key, Integer value) {
        sharedPref.edit().putInt(key, value).apply();
    }

    public static Integer getInteger(String key, Integer defValue) {
        return sharedPref.getInt(key, defValue);
    }

    public static void putBoolean(String key, boolean value) {
        sharedPref.edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return sharedPref.getBoolean(key, defValue);
    }

    public static void putLong(String key, long value) {
        sharedPref.edit().putLong(key, value).apply();
    }

    public static long getLong(String key, long defValue) {
        return sharedPref.getLong(key, defValue);
    }


    public static void putInt(String key, int value) {
        sharedPref.edit().putInt(key, value).apply();
    }

    public static int getInt(String key, int defValue) {
        return sharedPref.getInt(key, defValue);
    }

    public static void putString(String key, String value) {
        sharedPref.edit().putString(key, value).apply();
    }

    public static String getString(String key, String defValue) {
        return sharedPref.getString(key, defValue);
    }

    public static void putArray(boolean[][] array, String arrayName) {
        putInt(arrayName + "_size", array.length);

        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < array.length; j++)
                putBoolean(arrayName + "_" + i + "-" + j, array[i][j]);
    }

    public static boolean[][] getArray(String arrayName) {
        int size = SharedPref.getInt(arrayName + "_size", 0);
        boolean array[][] = new boolean[size][size];
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                array[i][j] = getBoolean(arrayName + "_" + i + "-" + j, false);

        return array;
    }

    public static void putArray(String key, ArrayList<Object> arrayList) {
        JsonArray value = (JsonArray) gson.toJsonTree(arrayList, new TypeToken<ArrayList<Object>>() {
        }.getType());
        SharedPref.putString(key, value.toString());
    }

    public static ArrayList<Object> getArray(String key, String defValue) {
        String strJson = sharedPref.getString(key, defValue);
        if (strJson == null) {
            return new ArrayList<>();
        }

        Type type = new TypeToken<ArrayList<Object>>() {
        }.getType();
        return gson.fromJson(strJson, type);
    }

    public static void saveToken(String token) {
        try {
            SharedPref.putString("t", Base64.encodeToString(encryptMsg(token, generateKey()), Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getToken() {
        try {
            return decryptMsg(Base64.decode(SharedPref.getString("t", null), Base64.DEFAULT), generateKey());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static SecretKey generateKey() {
        return new SecretKeySpec("mahavision".getBytes(), "AES");
    }

    private static byte[] encryptMsg(String message, SecretKey secret)
            throws Exception {
        /* Encrypt the message. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        return cipher.doFinal(message.getBytes("UTF-8"));
    }

    private static String decryptMsg(byte[] cipherText, SecretKey secret)
            throws Exception {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        return new String(cipher.doFinal(cipherText), "UTF-8");
    }
}
