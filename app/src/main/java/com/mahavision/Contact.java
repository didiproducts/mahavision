package com.mahavision;

import java.util.HashMap;
import java.util.Map;

public class Contact {

    private String id;
    private String firstName;
    private String lastName; //for server - only first letter
    private String email;
    private String phoneNumber;
    private String icon;


    public Contact(String id, String firstName, String lastName, String email, String phoneNumber, String icon) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getFullName() {
        return this.getFirstName() + " " + this.getLastName();
    }

    public Map<String, String> toHashMap() {
        Map<String, String> map = new HashMap<>();
        map.put("first name", this.firstName);
        map.put("last name", this.lastName.substring(0, 1)); // For server..
        map.put("email", this.email);
        map.put("phone number", this.phoneNumber);
        map.put("icon", this.icon);

        return map;
    }

}
