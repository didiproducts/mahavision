package com.mahavision;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;

public class ServerUtils {

    private String baseUrl = "https://aboard-tarp.glitch.me";

    public void AddContact(Context context, final Contact contact, final ServerCallback callback) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, baseUrl + "/RegisterNewUser",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        callback.onCallBack(response); // user id
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Error.Response", error.getMessage());
                        callback.onCallBack(error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return contact.toHashMap();
            }
        };
        queue.add(stringRequest);
    }

    public void getContact(Context context, final String id, final ServerCallback callback) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, baseUrl + "/contact/" + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        callback.onCallBack(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onCallBack(error.getMessage());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
