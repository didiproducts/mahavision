package com.mahavision;

import android.app.Application;

public class app extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPref.init(this);
    }
}
